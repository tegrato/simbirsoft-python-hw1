#! /usr/bin/env python
# -*- coding: utf-8 -*-
### Модуль TERMCOLOR установлен, в Windows работает, на Mac - нет :(

import sys
from termcolor import colored, cprint


## Надпись синим:
text = colored('Hello, Python!', 'blue')
print(text)

## Надпись зеленым с красной заливкой:
cprint('Hello, Python!', 'white', 'on_red')


## Цикл четных сиреневых чисел:
for i in range(1,30):
    if (i % 2) == 0:
        cprint(i, 'magenta', end=' ')
